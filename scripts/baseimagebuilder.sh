#!/usr/bin/bash
: <<'END'
Builds base image of Arch linux using buildar on Arch linux systems.
END

ARCH=amd64 # Manually set because ARM has an issue

baseimage=$(sudo buildah from scratch)
echo $baseimage
mntbase=$(sudo buildah mount $baseimage)
echo "y"|pacstrap -i -c $mntbase base busybox
buildah run $baseimage -- busybox --install -s
buildah unmount mntbase$

# Set configurations for the container
buildah config --author DagoRed
buildah config --arch $ARCH $baseimage
buildah config --label Name=archlinuxbase $baseimage 
# buildah config --variant v8 $baseimage  # Only needed for ARM
buildah config --entrypoint '["bash"]' $baseimage
buildah commit $baseimage archbase:$ARCHv1.0
buildah rm $baseimage
