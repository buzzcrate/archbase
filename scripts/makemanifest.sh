#!/usr/bin/bash
## Original script from a Nalin Dahyabhai presentation. 

listName=list
buildah manifest create $listName 

buildah manifest add --override-arch=arm64 --override-os=linux \
--os=linux --arch=arm64 --variant=v8 $listName docker://registry.gitlab.com/buzzcrate/archbase:arm64v1.0

buildah manifest add --override-arch=amd64 --override-os=linux \
--os=linux --arch=amd64 $listName docker://registry.gitlab.com/buzzcrate/archbase:amd64v1.0

buildah manifest inspect $listName|tee manifest.txt
# buildah manifest push $listName docker://registry.gitlab.com/buzzcrate/archbase

buildah rmi $listName

## Notes for manifest
# buildah manifest add takes arguments
#   --arch: mostly Go arch names
#   --os: linux
#   --os-version : mostly unused except for windows
#   --variant: mostly unused, except for ARM
#   --feactured: unused, in Docker format but reserved in OCI format
#   --os-features: unused, except maybe when OS is windows. 
