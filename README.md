# Arch Base 

Home for the Arch Linux and Arch Linux ARM base images. Contained are all 
scripts to produce base images via buildah and create a manifest. 

## Using containers

**Manual Containers**
* ARM image: registry.gitlab.com/buzzcrate/archbase:arm64v1.0
* AMD64 image: registry.gitlab.com/buzzcrate/archbase:amd64v1.0

**Automated Builds**
* ARM image: registry.gitlab.com/buzzcrate/archbase/arm64:latest
* AMD64 image: registry.gitlab.com/buzzcrate/archbase/amd64:latest

